# Intubation Station

A 'reverse faceshield' for protecting doctors and support staff from splashes during intubation.

(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and displayed for any purpose, but must acknowledge the Intubation Station project. Copyright is retained and must be preserved. The work is provided as-is; no warranty is provided, and users accept all liability.

## status
After gathering feedback from various medical professionals, we've moved to a frameless foldable design: https://gitlab.cba.mit.edu/alfonso/folding-intubation-box

This project will remain up for recordkeeping purposes. 

## need
A few recent discussions with a local intubator (i.e. a doctor) produced a memorable quote:

`intubation... is becoming the scariest thing for a lot of us.`

During intubation patients often cough, spraying sputum everywhere and putting front-line medical workers at significant risk. As cases ramp up in number and severity, this procedure will become more common for covid-19 patients. One proposed solution [from Dr. Hsien Yung Lai in Taiwan](https://sites.google.com/view/aerosolbox/design) encloses the patient's head in a clear plastic box, shown here:

![intubation_box_illustration.jpg](img/aerosolbox.jpg)

This design is constructed of glued polycarbonate or acrylic with an open back. The Intubation Station is designed to function similarly to Dr. Hsien Yung Lai's design, but improves fabricatability for FabLab scaling, reduces visual distortion, and reversibly assembles in a few seconds from a flat-pack kit.

## prototype

A prototype was constructed out of 6.5 mm HDPE (multi-shaded due to stock availability) and 0.5 mm PET-G. These materials are less fussy to work with than clear acrylic or polycarbonate, and a bit less expensive as well:

![intubation-station_prototype_1](img/intubation-station_prototype_1.jpg)

Lab personnel restrictions make photographing the box in use somewhat difficult without a bit of tripod work:

![intubation-station_prototype_2](img/intubation-station_prototype_2.jpg)

The frame snaps together using [MTM-style snap lock joints](http://mtm.cba.mit.edu/machines/mtm_snap-lock/index.html). These parts were designed around abrasive-free waterjet cutting so the current design lacks radii for a milling cutter. Please pardon the machining fuzz:

![intubation-station_prototype_3](img/intubation-station_prototype_3.jpg)

The shield was fabricated on a high-speed digital cutter with an oscillating carbide blade, but could be laser cut given a suitably large laser cutter. This piece has a number of tabs that fit in matching slots on the frame and are folded over to secure the shield. This part is a bit fussy to assemble, but the assembly is quite sturdy once it's together:

![intubation-station_prototype_4](img/intubation-station_prototype_4.jpg)

![intubation-station_durability](img/intubation-station_durability.mp4)

Fabrication files for 6.5 mm frame stock and 0.5 mm shield stock are located in ./model.

## early feedback and next steps
Dr. Achi Oren-Grinberg, an anesthesiologist from Beth Israel Lahey Health, stopped by the lab to provide initial thoughts on the prototype:

![achi_lobby](img/achi_lobby.jpg)

His feedback will go in to the next design iteration. Other changes are focused on fabricatability; first, the snap-locks will be modified to be ShopBot-able using a standard 1/8" end mill (shown here on the right with some experimental assist tabs to make disassembly easier):

![new_clips](img/new_clips.png)

The PETG shield configuration will be changed to wrap around the outside of the enclosure, and clip on either side using a simple flexural latch:

![new_latch](img/new_latch.jpg)

These changes should allow the Intubation Station to be assembled in less than a minute. Once the new prototype is constructed, more feedback will be gathered in a simulated clinical setting.

## 04/03 new clips

After some chatting about the click system to be able to mount and dismount the box, it has been proposed the following design.

![clip1](img/clip1.gif)

This system is composed by a pre-loaded arm that fits into a milled shape and clamp when it found its longitudinal position.


Different variants are being made, finally chosen the smoother and easiest one (the middle one):

For a system composed by two of them, the clamp works as:

![clip](img/option3.jpeg)
![dual](img/dual.gif)

The model manufactured and assembled look like the following.

![box](img/box.jpeg)
![sheetdetail](img/shieldetail.jpeg)

Inside the folder MODEL it can be found in the V2 folder the STEP file as well as dxf files for each side and a general arrangement separated by layers (internal mill and external mills) for a 4ft wide raw sheet. Recommended drill bit diameter from 2 - 3.3mm

## 06/04 sides view and doctor feedback

This mornin Zach told me that one of the very first feedback that a doctor gave to him was that there is a necessity to increase the vision in the lateral panels. To do that, we have to generate the lateral window as well as the PETG lateral protections. Also, this was a change of make a simpler version of the frontal flexural clamps.

![simplf](img/simp.png)

The angle in the lower part helps and made the assembly so satisfying.

Besides, this new item makes a second roll which is holding and locking as well the lateral windows.

The iteration no.3 of the design with the added lateral windows is the following:
![v3](img/assyv3.png)

Also the PETG protection has been re designed, adding some lateral holes to be inserted on the frontal flexural clamps to maintain the shape.

![shieldv3](img/shieldv3.png)

After manufactured and assembled, the mock up looks like this:


![mockupv3](img/mockupv3.jpeg)

Right now the mockup is in the hospital being tested by the doctors which will come to us with feedback. Looking forward to heard it.
